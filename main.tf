provider "aws" {
  region = var.region
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "ubuntu" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  
  user_data = <<-EOF
    #!/bin/bash
    sudo su
    apt update -y
    apt install mini-httpd -y
    systemctl start mini-httpd
    systemctl enable mini-httpd
    chown -R $USER /var/www/html
    # Create a simple HTML file
    echo "<h1>Hello World</h1>" > /var/www/html/index.html
  EOF
  
  tags = {
    Name = var.instance_name
  }
}

resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_security_group" "example1" {
  name_prefix = "d2gexample-sg-1"
  description = "Example security group 1"
  vpc_id      = aws_vpc.example.id

  # Ingress rules for the first security group
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "example2" {
  name_prefix = "d2gexample-sg-2"
  description = "Example security group 2"
  vpc_id      = aws_vpc.example.id

  # Ingress rules for the second security group
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "security_group_count" {
  value = length(aws_security_group.example1) + length(aws_security_group.example2)
}
